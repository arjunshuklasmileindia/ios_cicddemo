//
//  AppointmentScreenTxt.swift
//  SmileIndia
//
//  Created by Arjun  on 15/07/21.
//  Copyright © 2021 Na. All rights reserved.
//

import Foundation

enum AppointmentScreenTxt {
    
    static let pending = "Pending"
    static let confirm = "Confirmed"
    static let completed = "Completed"
    static let history = "History"
    
    static let ManageDocAppointement = "Manage Doctor Appointment"
    static let AppointmentStatus = "Appointment Status"
    
    static let refreshing = "Refreshing..."
    static let updateAccountDetailsreceivepayment = "Update your account details first to receive payments."
    
    static let InfoManageAppointement = "For each appointment ,10% of your consultation fee will be deducted as SmileIndia Service Charge"
    
    static let CancelAppointement = "Cancelled Appointments :"
    static let Appointments = "Appointments :"
    
    static let membersName = "Member's Name :"
    static let doctorsName = "Doctor's Name :"
    
    static let InPerson = "In-Person"
    static let eAppointement = "e-Appointment"
    static let AppointementDate = "Appointment Date :"
    static let AppointementTime = "Appointment Time :"
    static let BookingDate = "Booking Date :"
    static let reason = "Reason : "
    static let emailAt = "Email At : "
    static let status = "Status : "
    static let refund = "Refund"
    static let refundtaken = "Refund Taken"
    static let notAvilable = "Not Available"
    static let yourReview = "Your Review :"
    static let doctorReply = "Doctor Reply :"
    static let patientNtAvilable = "Patient is not available"
    static let doctorNtAvilable = "Doctor is not available"
    static let areYouSureDeleApointment = "Are you sure to delete this appointment?"
    static let areYouSureCancelApointment = "Are you sure to cancel this appointment?"
    static let ConfirmApointement = "Are you sure to confirm this appointment?"
    static let areYouSureCompleteApointment = "Are you sure to complete this appointment?"
    static let movedSucessfullyConfirm = "Appointment moved successfully to confirm section."
    static let movedSucessFullyComplete = "Appointment moved successfully to complete section."
    static let enterOtpMember = "Please enter OTP sent to member."
    static let submitt = "Submit"
    static let EnterOtp = "Enter OTP"
    static let OTPSentMember = "Please enter OTP sent to member"
    static let valid = "valid"
    static let movedSucessFullyHistory = "Appointment moved successfully to history section."
    static let AppointementSucessfullyDelete = "Appointment successfully deleted."
    static let prescriptonNtFound = "Prescription not found!."
    
    enum permissDenied {
        static let first = "If your privacy settings are preventing you to select more"
        static let sencond = "from your gallery. You can fix this by doing the following:"
        static let Third = "Click the Go button below to open the Vigorto Settings."
        static let fourth = "Click on Photos"
        static let fifth = "Select All Photos to enjoy limitless features of Vigorto"
    }
    
    static let permissionError = "Permission Error!"
    static let photos = "Photos"
    static let acceptpaylaterAPT = "Are you sure you want to Accept the Pay Later Appointment?"
    static let comingSoon = "Comming Soon!"
    static let areYoudoctorNtAvilable = "Are you sure doctor is not available?"
    static let areYoupatientNtAvilable = "Are you sure patient is not available?"
    static let markPatientNtavilable = "You have added the prescription, do you still want to mark the patient not available?"
    static let locationNtAvilable = "Location not available!"
    static let APTCancelSucesfully = "Appointment cancelled successfully."
    static let APTSucessDelete = "Appointment successfully deleted."
    static let prescriptionNotFound = "Prescription not found!."
    static let noFilesUploadedBy = "No Files uploaded by"
    static let pleaseUploadFiles = "Please upload Files."
    static let areYouSureDeleteFile = "Are you sure to delete this file ?"
    static let selectOptionForRefund = "Please select an option for refund :"
    static let initiateRefund = "INITIATE REFUND"
    static let vigortoCredits = "Vigorto Credits"
    static let memberInsurence = "MEMBER'S INSURANCE : "
    static let dismiss = "Dismiss"
    static let viewFull = "View Full"
    static let InsurencePlanName = "Insurance Plan Name : "
    static let confirmAppointementWithInsurence = "Are you sure to confirm this appointment with this insurance ?"
    
    // Upload VC
    
    static let chooseFile = "Please choose your file first"
    static let selectImgFrom = "Select Images From"
    static let camera = "Camera"
    static let Gallery = "Gallery"
    static let acessCameraDont = "Don't have camera access"
    static let selected = "You have selected"
    static let files = "files"
    static let fileuploadSucessFully = "Files Uploaded Successfully!"
    
    
    static let suretoDeleteFile = "Are you sure to delete this file ?"
    static let memberfile = "Member File"
    static let insurencecardImage = "Insurance Card Image"
    static let DoctorPrescriptionImage = "Doctor Prescription Image"
    
}




